import torch
import random
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import itertools

class learnable_highd_gaussian(nn.Module):
    def __init__(self,vals,initial_precision = None,max_precision = None,fixed_precision = True):
        super(learnable_highd_gaussian,self).__init__()

        self.dim = len(vals)
        self.vals = [np.array(x) for x in vals]

        self.vals_center  = np.array([(x[0] + x[-1])/2 for x in self.vals])
        self.vals_scale = np.array([x[-1] - x[0] for x in self.vals])

        self.vals_centered = np.array([(x-m)/s for (x,m,s) in zip(self.vals,self.vals_center,self.vals_scale)])

        self.register_buffer('grid',torch.Tensor(np.array(list(itertools.product(*self.vals_centered)))))
        self.true_grid = np.array(list(itertools.product(*self.vals)))

        self.mean = Parameter(torch.zeros(self.dim))

        precision_val = 5.0 if initial_precision is None else initial_precision
        precision_component = torch.sqrt(torch.eye(self.dim) * precision_val)
        if fixed_precision:
            self.register_buffer('precision_component',precision_component)
        else:
            self.precision_component = Parameter(precision_component)
            
        if max_precision is not None:
            self.max_precision_component = math.sqrt(self.max_precision)
        else:
            self.max_precision_component = None
    def get_probs(self):
        centered_grid = self.grid - self.mean
        precision_matrix = self.precision_component @ self.precision_component.transpose(1,0)
        unnormalized_probs = torch.sqrt(torch.det(precision_matrix)) * torch.exp(-0.5 * (centered_grid * (centered_grid @ precision_matrix)).sum(-1))
        probs = unnormalized_probs/unnormalized_probs.sum()
        return probs        

    def forward(self):
        self.mean.data.copy_(torch.clamp(self.mean.data,-0.5,0.5))                
        if self.max_precision_component is not None:
            self.precision_component.data.copy_(torch.clamp(self.precision_component.data,max = self.max_precision_component))

        probs = self.get_probs()
        
        position = torch.distributions.multinomial.Multinomial(1,probs = torch.clamp(probs,min = 1.0e-12)).sample().argmax().item()
           
        self.sample = self.grid[position].numpy() * self.vals_scale + self.vals_center
        
        self.logprob = torch.log(probs[position])
        return self.sample,self.logprob,position


def mean_and_cut(v_losses,cutoff_round):
    v_losses_means  = np.array([x[x != -np.inf].mean() for x in np.array(v_losses)[1:]])
    v_losses_means_cut = v_losses_means[cutoff_round:]
    return v_losses_means_cut
    
def get_hyper_loss(hparam_distro,val_losses,cutoff_round,hparam_per_round_history,logprob_history,no_reinforce_baseline,windowed_updates):

    val_losses_means_cut = mean_and_cut(val_losses,cutoff_round)
    loss = 0
    improvements = [0]
    if val_losses_means_cut.size > 1:
       improvements = - ((val_losses_means_cut[1:] / val_losses_means_cut[:-1]) - 1)
       mean_improvement = improvements.mean()
       print('*******mean improvement ',mean_improvement)
       improvement_tuple = [(hparam_distro.true_grid[hparam_per_round_history[cutoff_round+i]],imp) for i,imp in enumerate(improvements)]
       print(' improvements ',improvement_tuple)
       if no_reinforce_baseline:
           mean_improvement = 0.0
       updates_limit  = len(improvements)+1 if windowed_updates else 2
       for j in range(1,updates_limit):
           print('adjusting grid point {} with cost {}'.format(hparam_distro.true_grid[hparam_per_round_history[-j-1]],(improvements[-j] - mean_improvement)))
           loss = logprob_history[-j-1] * (improvements[-j] - mean_improvement)
    return loss,improvements[-1]
