import torch.utils.data
import torchvision
from torchvision import transforms,datasets
from torch.utils.data import Dataset, DataLoader
import numpy as np
import librosa
import os

def process_dataset(dataset,chunk_size = 1000):
    class_indices = []
    n_examples_per_class = []
    for i in range(len(dataset)):
        cls = int(dataset[i][1])
        while len(class_indices) <= cls:
              class_indices.append(np.zeros(chunk_size).astype('int32'))
              n_examples_per_class.append(0)
        if n_examples_per_class[cls] == class_indices[cls].size:
           class_indices[cls].resize(chunk_size + class_indices[cls].size)
        class_indices[cls][n_examples_per_class[cls]] = i
        n_examples_per_class[cls] += 1
    return [x[:y] for x,y in zip(class_indices,n_examples_per_class)]


class single_class_dataset(torch.utils.data.Dataset):
      processed_datasets = {}
      def __init__(self,parent_dataset,class_index,dataset_name):
          if not(single_class_dataset.processed_datasets):
              if os.path.isfile('./processed_datasets_cache'):
                  print('loading cached processed datasets')
                  single_class_dataset.processed_datasets = torch.load('./processed_datasets_cache')
          if dataset_name not in single_class_dataset.processed_datasets:
              single_class_dataset.processed_datasets[dataset_name] = process_dataset(parent_dataset)
              print('Updating cached processed datasets by '+repr(dataset_name))              
              torch.save(single_class_dataset.processed_datasets,'./processed_datasets_cache')
          else:
              print('using cached processed dataset ' + repr(dataset_name))
          self.class_indices= single_class_dataset.processed_datasets[dataset_name][class_index]
          self.parent_dataset = parent_dataset

      def __len__(self):
          return len(self.class_indices)

      def __getitem__(self,index):
          return self.parent_dataset[self.class_indices[index]]

def multi_class_dataset(parent_dataset,class_indices,dataset_name):
    sub_datasets = [single_class_dataset(parent_dataset,cls_idx,dataset_name) for cls_idx in class_indices]
    return torch.utils.data.ConcatDataset(sub_datasets)








class single_class_speech_dataset(torch.utils.data.Dataset):
    def __init__(self,transform,class_name,class_idx,base_folder,use_file=None,exclude_files = None,cache_in_memory = True ):
        self.transform = transform
        self.class_name = class_name
        self.class_idx = class_idx
        
        if use_file is not None:
            assert exclude_files is None, 'can not provide both use_file and exlcude_files arguments. One has to be None'
            self.class_files = single_class_speech_dataset.get_file_contents(base_folder,use_file,class_name)
        else:
            excluded_files_contents = []
            for exc_file in exclude_files:
                excluded_files_contents.extend(single_class_speech_dataset.get_file_contents(base_folder,exc_file,class_name))
            excluded_files_contents = set(excluded_files_contents)
            all_class_files = set([os.path.join(base_folder,class_name,x) for x in os.listdir(os.path.join(base_folder,class_name))])

            self.class_files = list(all_class_files - excluded_files_contents)

        self.data = [None] * len(self.class_files)
        self.cache_in_memory = cache_in_memory
        
    @staticmethod
    def get_file_contents(base_folder,file_name,class_name):
        file_name = os.path.join(base_folder,file_name)
        contents = []
        with open(file_name,'r') as f:
            for line in f.readlines():
                if line.split('/')[0] == class_name:
                    contents.append(os.path.join(base_folder,line.rstrip()))
        return contents

        
    def __len__(self):
        return len(self.class_files)
    def __getitem__(self,idx):
        if self.data[idx] is not  None:
            datapoint =  self.data[idx]
        else:
            datapoint = self.transform(self.class_files[idx])
            if self.cache_in_memory:
               self.data[idx] = datapoint
        return (datapoint,self.class_idx)
            
        
class audio_load_transform():
    def __init__(self,n_mels = 32,n_points = 16000):
        self.n_mels = n_mels
        self.n_points = n_points
    def __call__(self,x):
        samples,sr = librosa.load(x,sr = 16000)
        if samples.size  < self.n_points:
            samples = np.pad(samples, (0, self.n_points - samples.size), "constant")
        elif samples.size > self.n_points:
            samples = samples[:self.n_points]
        mels = librosa.feature.melspectrogram(samples,sr = sr,n_mels = self.n_mels)
        return torch.Tensor(librosa.power_to_db(mels, ref=np.max)).unsqueeze(0)

