import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict
import numpy as np
import math
from torch.nn.parameter import Parameter

class ReshapeBatch(nn.Module):
    """ A simple layer that reshapes its input and outputs the reshaped tensor """
    def __init__(self, *args):
        super(ReshapeBatch, self).__init__()
        self.args = args

    def forward(self, x):
        return x.view(x.size(0), *self.args)

    def __repr__(self):
        s = '{}({})'
        return s.format(self.__class__.__name__, self.args)


class cifar10_encoder(nn.Module):
    def __init__(self,dropout = 0.0):
        super(cifar10_encoder,self).__init__()
        self.stack = nn.ModuleList([nn.Identity(),
                                    nn.Conv2d(3, 32, kernel_size=5, padding=2),
                                    nn.MaxPool2d(2,return_indices = True),                           
                                    nn.ReLU(),
                                    nn.Dropout(dropout),
                                    nn.Conv2d(32, 64, kernel_size=5, padding=2),
                                    nn.MaxPool2d(2,return_indices = True),                            
                                    nn.ReLU(),                                    
                                    nn.Dropout(dropout),
                                    ReshapeBatch(-1),
                                    nn.Linear(4096,1024),
                                    nn.ReLU(),
                                    nn.Dropout(dropout),
                                    nn.Linear(1024,10,bias = False)
                                    ])
        self.taps = [0,3,7,11,13]
    def forward(self,x):
        tap_outputs = []
        max_pool_pos = []
        for i,layer in enumerate(self.stack):
            if isinstance(layer,nn.MaxPool2d):
               x,pos = layer(x)
               max_pool_pos.append(pos)
            else:
               x = layer(x) 
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,max_pool_pos,x

class cifar10_decoder(nn.Module):
    def __init__(self):
        super(cifar10_decoder,self).__init__()
        self.stack = nn.ModuleList([nn.Linear(10,1024),
                                    nn.ReLU(),#1
                                    nn.Linear(1024,4096),
                                    nn.ReLU(),                         
                                    ReshapeBatch(64,8,8),#4
                                    nn.MaxUnpool2d(2),
                                    nn.Conv2d(64,32,kernel_size = 5,padding= 2),
                                    nn.ReLU(),
                                    nn.MaxUnpool2d(2),
                                    nn.Conv2d(32,3,kernel_size = 5,padding= 2)
                                    ])
        self.taps = [1,4,7,9]
    def forward(self,x,unpool_positions):
        tap_outputs = []
        unpool_idx = len(unpool_positions)-1
        for i,layer in enumerate(self.stack):
            if isinstance(layer,nn.MaxUnpool2d):
               x = layer(x,unpool_positions[unpool_idx])
               unpool_idx -=1 
            else:
               x = layer(x) 
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,x


  
class cifar10_model(nn.Module):
    def __init__(self):
        super(cifar10_model, self).__init__()
        self.stack = nn.Sequential(nn.Identity(),
                                    nn.Conv2d(3, 32, kernel_size=5, padding=2),
                                    nn.MaxPool2d(2),                           
                                    nn.ReLU(),
                                    nn.Conv2d(32, 64, kernel_size=5, padding=2),
                                    nn.MaxPool2d(2),                            
                                    nn.ReLU(),                                    
                                    ReshapeBatch(-1),
                                    nn.Linear(4096,1024),
                                    nn.ReLU(),
                                    nn.Linear(1024,10,bias = False)
                                    )

    def forward(self, x):
        x = self.stack(x)
        return x
    


class mnist_encoder(nn.Module):
    def __init__(self,n_hidden):
        super(mnist_encoder,self).__init__()
        nonlin  = nn.ReLU
        self.stack = nn.ModuleList([ReshapeBatch(-1),
                                    nn.Linear(784,n_hidden),
                                    nonlin(),
                                    nn.Linear(n_hidden,n_hidden),
                                    nonlin(),
                                    nn.Linear(n_hidden,10)])
        self.taps = [0,2,4,5]
    def forward(self,x):
        tap_outputs = []
        for i,layer in enumerate(self.stack):
            x = layer(x)
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,[None],x

class mnist_decoder(nn.Module):
    def __init__(self,n_hidden):
        super(mnist_decoder,self).__init__()
        nonlin  = nn.ReLU
        self.stack = nn.ModuleList([nn.Linear(10,n_hidden),
                                    nonlin(),
                                    nn.Linear(n_hidden,n_hidden),
                                    nonlin(),
                                    nn.Linear(n_hidden,784)])
        self.taps = [1,3,4]

    def forward(self,x):
        tap_outputs = []
        for i,layer in enumerate(self.stack):
            x = layer(x)
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,x

class mnist_model(nn.Module):
    def __init__(self,n_hidden):
        nonlin = nn.ReLU
        super(mnist_model,self).__init__()
        self.stack =  nn.Sequential(ReshapeBatch(-1),
                              nn.Linear(784,n_hidden),
                              nonlin(),
                              nn.Linear(n_hidden,n_hidden),
                              nonlin(),
                              nn.Linear(n_hidden,10))
    def forward(self,x):
        return self.stack(x)
    

  

class speech_model(nn.Module):
    def __init__(self):
        super(speech_model,self).__init__()
        self.stack = nn.Sequential( nn.LayerNorm([32,32]),
                                    nn.Conv2d(1, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.MaxPool2d(2),                                  
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.MaxPool2d(2),                                  
                                    ReshapeBatch(-1),
                                    nn.Linear(4096,1024),
                                    nn.ReLU(),
                                    nn.Linear(1024,10,bias = False)
                                    )
    def forward(self,x):
        return self.stack(x)


class speech_encoder(nn.Module):
    def __init__(self):
        super(speech_encoder,self).__init__()
        self.stack = nn.Sequential( nn.LayerNorm([32,32]),
                                    nn.Conv2d(1, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.MaxPool2d(2,return_indices = True),                         
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.Conv2d(64, 64, kernel_size=3, padding=1),
                                    nn.ReLU(),
                                    nn.MaxPool2d(2,return_indices = True),                            
                                    ReshapeBatch(-1),
                                    nn.Linear(4096,1024),
                                    nn.ReLU(),
                                    nn.Linear(1024,10,bias = False)
                                    )
        self.taps = [0,2,5,7,10,13,14]
    def forward(self,x):
        tap_outputs = []
        max_pool_pos = []
        for i,layer in enumerate(self.stack):
            if isinstance(layer,nn.MaxPool2d):
               x,pos = layer(x)
               max_pool_pos.append(pos)
            else:
               x = layer(x) 
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,max_pool_pos,x


class speech_decoder(nn.Module):
    def __init__(self):
        super(speech_decoder,self).__init__()
        self.stack = nn.ModuleList([nn.Linear(10,1024),#0
                                    nn.ReLU(),#1
                                    nn.Linear(1024,4096),#2
                                    nn.ReLU(),           #3              
                                    ReshapeBatch(64,8,8),#4
                                    nn.MaxUnpool2d(2),#5
                                    nn.Conv2d(64,64,kernel_size = 5,padding= 2),#6
                                    nn.ReLU(),#7
                                    nn.Conv2d(64,64,kernel_size = 5,padding= 2),#8
                                    nn.ReLU(),#9
                                    nn.MaxUnpool2d(2),#10
                                    nn.Conv2d(64,64,kernel_size = 5,padding= 2),#11
                                    nn.ReLU(),#12
                                    nn.Conv2d(64,1,kernel_size = 5,padding= 2)#13
                                    ])
        self.taps = [1,4,7,9,12,13]
    def forward(self,x,unpool_positions):
        tap_outputs = []
        unpool_idx = len(unpool_positions)-1
        for i,layer in enumerate(self.stack):
            if isinstance(layer,nn.MaxUnpool2d):
               x = layer(x,unpool_positions[unpool_idx])
               unpool_idx -=1 
            else:
               x = layer(x) 
            if i in self.taps:
                tap_outputs.append(x)
        return tap_outputs,x
  
  
  

